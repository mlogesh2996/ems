import React, { useContext, useState, useEffect, useReducer } from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Backdrop from "@mui/material/Backdrop";
import { Button, TextField } from "@material-ui/core";
import style from "./employeeform.module.scss";
import { useRouter } from "next/router";
import employeeList from "../Dashboard/fixture";

function EmployeeForm(props: any) {
  const router = useRouter();
  const [formInput, setFormInput] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      name: "",
      dob: "",
      address: "",
      role: "",
      salary: "",
      experience: "",
    }
  );

  const [formInputHasError, setFormInputHasError] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      name: true,
      dob: true,
      address: true,
      role: true,
      salary: true,
      experience: true,
    }
  );
  const [enableValidation, setEnableValidation] = useState(false);

  const handleInput = (evt) => {
    const name = evt.target.name;
    const newValue = evt.target.value;
    setFormInput({ [name]: newValue });
    if (!newValue) {
      setFormInputHasError({ [name]: true });
    } else {
      setFormInputHasError({ [name]: false });
    }
  };

  const handleSubmit = (evt) => {
    evt.preventDefault();

    let isAllFieldsValid = true;
    setEnableValidation(true);
    Object.keys(formInputHasError).forEach((item) => {
      if (formInputHasError[item]) {
        isAllFieldsValid = false;
      }
    });
    console.log("SAVE: ", formInput);
    if (isAllFieldsValid) {
      employeeList.employeeList.push(formInput);
      localStorage.setItem("employees", JSON.stringify(employeeList));
      goToDashboard();
    }
  };

  const goToDashboard = () => {
    router.push({
      pathname: `/employee`,
    });
  };

  return (
    <>
      <Container
        fixed
        style={{ minWidth: "800px" }}
        className={`${style.containercustomadjustment} `}
      >
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={false}
        ></Backdrop>
        <Typography
          component="div"
          style={{
            paddingTop: "80px",
            paddingLeft: "90px",
            marginTop: "25px",
            backgroundColor: "#ffffff",
            height: "900px",
            boxShadow:
              "0px 0px 6px 2px rgb(0 0 0 / 16%), 1px -5px 15px 2px rgb(0 0 0 / 12%)",
            borderRadius: "0.25rem",
          }}
        >
          <Typography
            variant="h5"
            component="h3"
            style={{ textAlign: "center" }}
          >
            Add Employee Details
          </Typography>

          <form onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              <Grid item xs={12} className="mt-5">
                <TextField
                  label="Name"
                  id="margin-normal"
                  name="name"
                  // defaultValue={formInput.name}
                  error={enableValidation && formInputHasError.name}
                  onChange={handleInput}
                  style={{ width: "70%" }}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  label="Address"
                  id="outlined-multiline-static"
                  multiline
                  rows={4}
                  name="address"
                  // defaultValue={formInput.name}
                  error={enableValidation && formInputHasError.address}
                  variant="outlined"
                  style={{ width: "70%" }}
                  onChange={handleInput}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label="DOB"
                  id="margin-normal"
                  name="dob"
                  type="date"
                  defaultValue={formInput.dob}
                  error={enableValidation && formInputHasError.dob}
                  style={{ width: "70%" }}
                  onChange={handleInput}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  label="Role"
                  id="margin-normal"
                  name="role"
                  defaultValue={formInput.role}
                  error={enableValidation && formInputHasError.role}
                  style={{ width: "70%" }}
                  onChange={handleInput}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  label="salary"
                  id="margin-normal"
                  name="salary"
                  type="number"
                  inputProps={{
                    step: "0.1",
                    min: 0,
                  }}
                  helperText="Salary in Lakh"
                  defaultValue={formInput.salary}
                  error={enableValidation && formInputHasError.salary}
                  style={{ width: "70%" }}
                  onChange={handleInput}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  label="Experience"
                  id="margin-normal"
                  name="experience"
                  type="number"
                  inputProps={{
                    step: "0.1",
                    min: 0,
                  }}
                  helperText="Experience in years"
                  defaultValue={formInput.experience}
                  error={enableValidation && formInputHasError.experience}
                  style={{ width: "70%" }}
                  onChange={handleInput}
                  variant="outlined"
                />
              </Grid>

              <Grid
                item
                xs={12}
                className="mt-4"
                style={{ textAlign: "center" }}
              >
                <Button
                  type="submit"
                  className={style.custombtn}
                  variant="contained"
                  color="primary"
                >
                  Save
                </Button>
              </Grid>
            </Grid>
          </form>
        </Typography>
      </Container>
    </>
  );
}

export { EmployeeForm };
export default EmployeeForm;
