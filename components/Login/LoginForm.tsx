import React, { useContext, useState, useEffect, useReducer } from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";
import { Button, TextField } from "@material-ui/core";
import style from "./loginform.module.scss";
import { useRouter } from "next/router";
import Cookies from "js-cookie";

function LoginForm(props: any) {
  const router = useRouter();
  const [formInput, setFormInput] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      username: "",
      password: "",
    }
  );

  const [formInputHasError, setFormInputHasError] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      username: true,
      password: true,
    }
  );
  const [enableValidation, setEnableValidation] = useState(false);

  const handleInput = (evt) => {
    const name = evt.target.name;
    const newValue = evt.target.value;
    setFormInput({ [name]: newValue });
    if (!newValue) {
      setFormInputHasError({ [name]: true });
    } else {
      setFormInputHasError({ [name]: false });
    }
  };

  const handleSubmit = (evt) => {
    evt.preventDefault();

    let isAllFieldsValid = true;
    setEnableValidation(true);
    Object.keys(formInputHasError).forEach((item) => {
      if (formInputHasError[item]) {
        isAllFieldsValid = false;
      }
    });

    console.log("formInput: ", formInput);
    let cookieData = JSON.parse(Cookies.get("credentials"));
    if (isAllFieldsValid) {
      if (
        cookieData?.username === formInput.username &&
        cookieData?.password === formInput.password
      ) {
        console.log("Login Success");
        alert("Login Success");
        goToDashboard();
      } else {
        console.log("Invalid credential");
        alert("Invalid credential! Please register");
      }
    }
  };

  const goToRegister = () => {
    router.push({
      pathname: `/register`,
    });
  };

  const goToDashboard = () => {
    router.push({
      pathname: `/employee`,
    });
  };

  return (
    <>
      <Container
        fixed
        style={{ minWidth: "800px" }}
        className={`${style.containercustomadjustment} `}
      >
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={false}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <h1 style={{ textAlign: "center", marginTop: "2rem" }}>Login</h1>
        <Grid container spacing={3}>
          <Grid item xs={6} className="mt-5 p-0">
            <Typography
              component="div"
              style={{
                paddingTop: "35px",
                paddingLeft: "55px",
                marginTop: "25px",
                backgroundColor: "#ffffff",
                height: "365px",
                boxShadow:
                  "0px 0px 0px 1px rgb(0 0 0 / 16%), 0px 0px 0px 0px rgb(0 0 0 / 12%)",
                borderRadius: "0.25rem",
                width: "100%",
              }}
            >
              <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                  <Grid item xs={12} className="mt-5">
                    <TextField
                      label="Username"
                      id="margin-normal"
                      name="username"
                      // defaultValue={formInput.name}
                      error={enableValidation && formInputHasError.username}
                      // helperText="username"
                      onChange={handleInput}
                      style={{ width: "80%" }}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      label="Password"
                      id="margin-normal"
                      name="password"
                      // defaultValue={formInput.name}
                      error={enableValidation && formInputHasError.password}
                      // helperText="Password"
                      onChange={handleInput}
                      style={{ width: "80%" }}
                      variant="outlined"
                    />
                  </Grid>

                  <Grid
                    container
                    xs={12}
                    className="mt-1 pl-2"
                    style={{ textAlign: "initial" }}
                  >
                    <Grid item xs={6}>
                      <Button
                        type="submit"
                        className={style.custombtn}
                        variant="contained"
                        color="primary"
                      >
                        Login
                      </Button>
                    </Grid>
                    <Grid item xs={6}>
                      <h6
                        style={{
                          marginTop: "12px",
                          color: "#4273c6",
                          fontWeight: 600,
                          fontSize: "0.8rem",
                        }}
                        className={style.register}
                        onClick={goToRegister}
                      >
                        Need an account?
                      </h6>
                    </Grid>
                  </Grid>
                </Grid>
              </form>
            </Typography>
          </Grid>
          <Grid item xs={6} className="mt-5 p-0">
            <Typography
              component="div"
              style={{
                paddingTop: "35px",
                marginTop: "25px",
                textAlign: "center",
                backgroundColor: "#f9f9fa",
                height: "365px",
                boxShadow:
                  "0px 0px 0px 1px rgb(0 0 0 / 16%), 0px 0px 0px 0px rgb(0 0 0 / 12%)",
                borderRadius: "0.25rem",
                width: "100%",
              }}
            >
              <div
                className="mt-3"
                style={{
                  textAlign: "center",
                  fontWeight: 600,
                  fontFamily: "cursive",
                  fontSize: "large",
                  paddingBottom: "2rem",
                }}
              >
                Employee Management System
              </div>
              **Login to view employee details**
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}

export { LoginForm };
export default LoginForm;
