import React, { useState, useEffect } from "react";
import style from "./dashboard.module.scss";
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import employeeList from "./fixture";
import { useRouter } from "next/router";
import { Button } from "@material-ui/core";

function Dashboard() {
  const router = useRouter();
  const [empList, setEmpList] = useState(employeeList);

  const goToEmp = (empId: any) => {
    router.push({
      pathname: `/employee/${empId}`,
      query: {
        employeeId: empId,
      },
    });
  };

  console.log("empList: ", empList);
  const onAddEmp = (event) => {
    event.preventDefault();
    router.push({
      pathname: `/employee/edit/add`,
    });
  };

  useEffect(() => {
    let employeeListFromLS = JSON.parse(localStorage.getItem("employees"));
    if (
      employeeListFromLS?.employeeList &&
      empList.employeeList.length < employeeListFromLS.employeeList.length
    ) {
      setEmpList(employeeListFromLS);
    }
  }, [empList]);

  const renderCard = (emp: any, indx: any) => {
    console.log("emp: ", emp);
    return (
      <div className={`card ${style.customcard}`} onClick={() => goToEmp(indx)}>
        <div style={{ height: "170px" }} className="card-body">
          <h3
            style={{
              fontStyle: "normal",
              fontWeight: 500,
              fontSize: "16px",
            }}
            className={`card-title ${style.empTitle} mb-1`}
          >
            {emp.name}
          </h3>
          {emp?.role && (
            <p
              style={{
                fontSize: "13px",
                fontWeight: 500,
                color: "#28a745",
                fontStyle: "initial",
              }}
              className={`card-text  ${style.empTitle} mb-1`}
            >
              {emp.role}
            </p>
          )}
          {emp?.dob && (
            <div
              style={{
                fontSize: "0.90rem",
                fontStyle: "normal",
                fontWeight: 500,
                color: "#6d6d6d",
              }}
            >
              {"DOB: "}
              {emp.dob}
            </div>
          )}
          {emp?.address && (
            <div
              style={{
                fontSize: "0.90rem",
                fontStyle: "normal",
                fontWeight: 500,
                color: "#6d6d6d",
              }}
            >
              {"Address: "}
              {emp.address}
            </div>
          )}
          {emp?.experience && (
            <div
              style={{
                fontSize: "0.90rem",
                fontStyle: "normal",
                fontWeight: 500,
                color: "#6d6d6d",
              }}
            >
              {"Experience: "}
              {emp.experience} Years
            </div>
          )}
          {emp?.salary && (
            <div
              style={{
                fontSize: "0.90rem",
                fontStyle: "normal",
                fontWeight: 500,
                color: "#6d6d6d",
              }}
            >
              {"Salary: "}
              {emp.salary} Lakh
            </div>
          )}
        </div>
      </div>
    );
  };

  return (
    <>
      <MDBContainer className={`${style.containercustomsm}`}>
        <h1
          style={{ fontWeight: 400, textAlign: "center", paddingTop: "2rem" }}
        >
          All Employees
          <div style={{ float: "right" }}>
            <Button
              className={style.custombtn}
              variant="contained"
              color="primary"
              onClick={onAddEmp}
            >
              Add employee
            </Button>
          </div>
        </h1>
        <MDBRow>
          {empList.employeeList?.length > 0 ? (
            empList.employeeList?.map((emp: any, index: number) => {
              return (
                <MDBCol
                  className={`${style.colextrasm}`}
                  xs="4"
                  sm="4"
                  md="4"
                  lg="3"
                  xl="2"
                  middle
                >
                  {renderCard(emp, index)}
                </MDBCol>
              );
            })
          ) : (
            <MDBCol
              style={{ textAlign: "center" }}
              className={`mt-5 ${style.colextrasm}`}
              size="12"
              middle
            >
              No Employee Data
            </MDBCol>
          )}
        </MDBRow>
      </MDBContainer>
    </>
  );
}

export { Dashboard };
export default Dashboard;
