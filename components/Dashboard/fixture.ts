var employeeList = [
  {
    id: 0,
    name: "Logesh M",
    dob: "02/09/1996",
    address: "Salem, TN",
    role: "Developer",
    salary: "5",
    experience: "2.5",
  },
  {
    id: 1,
    name: "Kumar M",
    dob: "04/09/1996",
    address: "Chennai, TN",
    role: "Lead",
    salary: "25",
    experience: "10",
  },
  {
    id: 2,
    name: "Sam M",
    dob: "01/09/1996",
    address: "Salem, TN",
    role: "Senior Dev",
    salary: "15",
    experience: "8",
  },
];

export { employeeList };
export default { employeeList };
