import React, { useContext, useState, useEffect, useReducer } from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";
import { Button, TextField } from "@material-ui/core";
import style from "./registerform.module.scss";
import { useRouter } from "next/router";
import Cookies from "js-cookie";

function RegisterForm(props: any) {
  const router = useRouter();

  const [formInput, setFormInput] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      username: "",
      password: "",
    }
  );

  const [formInputHasError, setFormInputHasError] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      username: true,
      password: true,
    }
  );
  const [enableValidation, setEnableValidation] = useState(false);

  const handleInput = (evt) => {
    const name = evt.target.name;
    const newValue = evt.target.value;
    setFormInput({ [name]: newValue });
    if (!newValue) {
      setFormInputHasError({ [name]: true });
    } else {
      setFormInputHasError({ [name]: false });
    }
  };

  const handleSubmit = (evt) => {
    evt.preventDefault();

    let data = { formInput };
    let isAllFieldsValid = true;
    setEnableValidation(true);
    Object.keys(formInputHasError).forEach((item) => {
      if (formInputHasError[item]) {
        isAllFieldsValid = false;
      }
    });

    if (isAllFieldsValid) {
      alert("Register Successful");
      Cookies.set("credentials", JSON.stringify(formInput));
      goToLogin();
    }
  };

  const goToLogin = () => {
    router.push({
      pathname: `/login`,
    });
  };

  return (
    <>
      <Container
        fixed
        style={{ minWidth: "800px" }}
        className={`${style.containercustomadjustment} `}
      >
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={false}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <h1 style={{ textAlign: "center", marginTop: "2rem" }}>Register</h1>
        <Grid container spacing={3}>
          <Grid item xs={3} className="mt-5 p-0" />
          <Grid item xs={5} className="mt-5 p-0">
            <Typography
              component="div"
              style={{
                paddingTop: "35px",
                paddingLeft: "55px",
                marginTop: "25px",
                backgroundColor: "#ffffff",
                height: "340px",
                boxShadow:
                  "0px 0px 0px 1px rgb(0 0 0 / 16%), 0px 0px 0px 0px rgb(0 0 0 / 12%)",
                borderRadius: "0.25rem",
                width: "550px",
              }}
            >
              <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                  <Grid item xs={12}>
                    <TextField
                      label="Username"
                      id="margin-normal"
                      name="username"
                      // defaultValue={formInput.name}
                      error={enableValidation && formInputHasError.username}
                      // helperText="username"
                      onChange={handleInput}
                      style={{ width: "80%" }}
                      variant="outlined"
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <TextField
                      label="Password"
                      id="margin-normal"
                      name="password"
                      // defaultValue={formInput.name}
                      error={enableValidation && formInputHasError.password}
                      // helperText="Password"
                      onChange={handleInput}
                      style={{ width: "80%" }}
                      variant="outlined"
                    />
                  </Grid>

                  <Grid
                    container
                    xs={12}
                    className="mt-3 pl-2"
                    style={{ textAlign: "initial" }}
                  >
                    <Grid item xs={6}>
                      <Button
                        type="submit"
                        className={style.custombtn}
                        variant="contained"
                        color="primary"
                      >
                        Register
                      </Button>
                    </Grid>
                    <Grid item xs={6}>
                      <h6
                        style={{
                          marginTop: "12px",
                          color: "#4273c6",
                          fontWeight: 600,
                          fontSize: "0.8rem",
                        }}
                        className={style.login}
                        onClick={goToLogin}
                      >
                        Already have account (Sign in)
                      </h6>
                    </Grid>
                  </Grid>
                </Grid>
              </form>
            </Typography>
          </Grid>
          <Grid item xs={4} className="mt-5 p-0" />
        </Grid>
      </Container>
    </>
  );
}

export { RegisterForm };
export default RegisterForm;
