import { LoginForm } from "../../components/Login/LoginForm";

export default function login() {
  return (
    <div>
      <LoginForm />
    </div>
  );
}
