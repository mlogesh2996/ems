import { RegisterForm } from "../../components/Register/RegisterForm";

export default function register() {
  return (
    <div>
      <RegisterForm />
    </div>
  );
}
