import { Dashboard } from "../../components/Dashboard/Dashboard";

export default function employees() {
  return (
    <div
      style={{
        background: "#f1f1f1",
        minHeight: "1200px",
        paddingTop: "30px",
        paddingBottom: "45px",
        minWidth: "800px",
      }}
    >
      <Dashboard />
    </div>
  );
}
