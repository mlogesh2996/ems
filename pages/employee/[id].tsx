import { useRouter } from "next/router";
import employeeList from "../../components/Dashboard/fixture";
export default function employeeDetails() {
  const router = useRouter();
  console.log(employeeList.employeeList);
  const empId = router?.query?.id || 0;
  let data: any;
  if (empId > 2) {
    //Take from LocalStorage
    let localStoreData = JSON.parse(localStorage.getItem("employees"));
    data = localStoreData.employeeList[empId];
  } else {
    //Take from fixture
    data = employeeList.employeeList[empId];
  }

  return (
    <div>
      <h1 style={{ textAlign: "center", margin: "2rem" }}>
        Employee {router?.query?.id}
      </h1>
      <h2>Selected Employee Details: {JSON.stringify(data)}</h2>
    </div>
  );
}
