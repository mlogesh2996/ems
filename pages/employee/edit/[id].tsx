import EmployeeForm from "../../../components/EmployeeForm/EmployeeForm";

export default function edit() {
  return (
    <div
      style={{
        background: "#f1f1f1",
        minHeight: "1200px",
        paddingTop: "30px",
        paddingBottom: "45px",
        minWidth: "800px",
      }}
    >
      <EmployeeForm />
    </div>
  );
}
